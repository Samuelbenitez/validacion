import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class TestEcuacion {

	@Test
	void TestEcuacion1() throws Exception {
		Ecuacion ecuacion1=new Ecuacion(1);
		assertEquals(4.0,ecuacion1.cuadratica(-2));
			
	}
	@Test
	void TestEcuacion2() throws Exception {
		Ecuacion ecuacion2=new Ecuacion(3, 6);
		assertEquals( 0.0,ecuacion2.cuadratica(0));
			
	}
	@Test
	void TestEcuacion3() throws Exception {
		Ecuacion ecuacion3=new Ecuacion(7, 2,9);
		assertEquals(18.0, ecuacion3.cuadratica(1));
			
	}
	
	@Test
	void TestEcuacion4() throws Exception {
		Ecuacion ecuacion4=new Ecuacion(-2);
		Assertions.assertThrows(Exception.class,
				() -> ecuacion4.raiz(3) );

	}
	
	@Test
	void TestEcuacion5() throws Exception {
		Ecuacion ecuacion5=new Ecuacion(7,5);
		assertEquals( -15.291502622129181, ecuacion5.raiz(-2) );
			
	}
	
	@Test
	void TestEcuacion6() throws Exception {
		Ecuacion ecuacion5=new Ecuacion(3,-2,0);
		assertEquals(0.0, ecuacion5.raiz(0));
			
	}

}
