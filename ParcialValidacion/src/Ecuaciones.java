
public class Ecuaciones {

	public static void main(String[] args) throws Exception {
		try {
		Ecuacion ecuacion1=new Ecuacion(1);
		Ecuacion ecuacion2=new Ecuacion(3,6);
		Ecuacion ecuacion3=new Ecuacion(7,2,9);
		
		System.out.println("Ecuacion 1:" + ecuacion1.cuadratica(-2));
		System.out.println("Ecuacion 2:" + ecuacion2.cuadratica(0));
		System.out.println("Ecuacion 3:" + ecuacion3.cuadratica(1));
		
		
		Ecuacion ecuacion4=new Ecuacion(-1);
		Ecuacion ecuacion5=new Ecuacion(7, 5);
		Ecuacion ecuacion6=new Ecuacion(3, -2, 0);
		
		
		System.out.println("Ecuacion 5:" + ecuacion5.raiz(-2));
		System.out.println("Ecuacion 6:" + ecuacion6.raiz(0));
		System.out.println("Ecuacion 4:" + ecuacion4.raiz(3));
		}catch(Exception e) {
			System.out.println(e.getMessage());
			
		}
		
		

	}

}
